FROM qgis/qgis:final-3_34_3

ARG USER_NAME=qgis-tester
ARG PLUGIN_NAME=qgisapify
ARG TESTS_ROOT

# headless QGIS
ENV QT_QPA_PLATFORM=offscreen

RUN pip3 install --upgrade --force-reinstall numpy

# add new regular user and run tests under his privileges
RUN useradd -ms /bin/bash $USER_NAME &&\
    apt-get update &&\
    apt-get install -y build-essential python3-venv &&\
    pip3 install --upgrade numpy


USER $USER_NAME

WORKDIR /home/${USER_NAME}

COPY --chown=${USER_NAME}:${USER_NAME} ./requirements /home/${USER_NAME}/requirements/

RUN mkdir -p /home/${USER_NAME}/requirements /home/${USER_NAME}/tests /home/${USER_NAME}/${PLUGIN_NAME} &&\
    pip3 install pytest &&\
    pip3 install -r requirements/tests.txt

COPY --chown=${USER_NAME}:${USER_NAME} ./$TESTS_ROOT ./$PLUGIN_NAME  ./setup.cfg ./tests/__init__.py  /home/${USER_NAME}/

CMD python3 -m pytest -vvl $TESTS_ROOT