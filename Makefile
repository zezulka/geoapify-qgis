# verify that all tools are available at the local system
REQUIRED_EXECUTABLES = make python3 flake8 docker-compose pyrcc5
EXECUTABLES_VERIFICATION_RESULT := $(foreach exec,${REQUIRED_EXECUTABLES},\
        $(if $(shell which $(exec)),some string,$(error "No $(exec) in PATH")))

SNAPSHOT_VER = snapshot
QGIS_PLUGIN_NAME = qgisapify

PATH_TO_PROJECT = ~/git/${QGIS_PLUGIN_NAME}
PATH_TO_QGIS_PLUGINS = ~/.local/share/QGIS/QGIS3/profiles/default/python/plugins
PATH_TO_QGIS_INSTALLATION = ${PATH_TO_QGIS_PLUGINS}/${QGIS_PLUGIN_NAME}

default: test install-locally clean-up

install-locally: qgis-plugin
	rm -rf ${PATH_TO_QGIS_INSTALLATION} &&\
	unzip -q -o ${QGIS_PLUGIN_NAME}.${SNAPSHOT_VER} -d ${PATH_TO_QGIS_PLUGINS}

qgis-plugin:
  # qgis-plugin-ci ignores files outside of version control ¯\_(ツ)_/¯
	git add . &&\
	qgis-plugin-ci package ${SNAPSHOT_VER} --allow-uncommitted-changes

test: unit-test integration-test

unit-test:
	docker-compose build qgis-unit-test &&\
 	docker-compose run qgis-unit-test

integration-test:
	docker-compose build qgis-integration-test &&\
    docker-compose run qgis-integration-test

clean-up:
	find . -name "*.pyc" -o -name "Ui_.*" -o -name "resources_*.py" -exec rm \{\} \; &&\
	rm  ${QGIS_PLUGIN_NAME}.${SNAPSHOT_VER}.zip &&\
	rm -rf venv-unit-tests