import pytest
from PyQt5.QtCore import QSettings
from qgis._core import QgsSettings
from qgis.gui import QgisInterface

from qgisapify.constants import QGIS_PLUGIN_NAME
from qgisapify.geoapify_plugin import GeoapifyPlugin


@pytest.fixture
def qgs_settings() -> QgsSettings:
    s = QgsSettings(QSettings.SystemScope, organization=QGIS_PLUGIN_NAME, application=QGIS_PLUGIN_NAME)
    s.setValue(f"/{QGIS_PLUGIN_NAME}/geoapify/api/urlprefix", "http://wiremock:8443")
    s.setValue('locale/userLocale', "en_US")
    return s


@pytest.fixture(scope="function")
def qgisapify(qgs_settings, qgis_iface: QgisInterface) -> GeoapifyPlugin:
    """
    Main fixture of integration tests.
    Contains a reference to the initialized Eagris plugin.
    """
    plugin = GeoapifyPlugin(qgis_iface)
    plugin.initGui()
    plugin.run()
    return plugin
