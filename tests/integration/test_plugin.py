from PyQt5.QtCore import Qt
from qgis._core import Qgis, QgsApplication

from qgisapify.geoapify_plugin import GeoapifyPlugin


def test_form_submit_fills_result_table_view(qgisapify: GeoapifyPlugin,
                                             qtbot):
    qtbot.keyClicks(
        qgisapify.dockwidget.addressLineEdit,
        "Wimbledon, UK"
    )
    qtbot.keyClicks(
        qgisapify.dockwidget.apiKeyLineEdit,
        "615fefff9d32d38057bf33d8a94eecff"
    )
    qtbot.mouseClick(
        qgisapify.dockwidget.searchButton,
        Qt.MouseButton.LeftButton
    )
    qtbot.waitUntil(lambda: qgisapify.dockwidget.resultTableView.model() is not None)
    assert qgisapify.dockwidget.resultTableView.model()._data.to_json() == '{"type": "FeatureCollection", "features": [{"id": "0", "type": "Feature", "properties": {"name": "Abbey Road", "street": "Abbey Road", "city": "London"}, "geometry": {"type": "Point", "coordinates": [0.0736776, 51.5374693]}}, {"id": "1", "type": "Feature", "properties": {"name": "Abbey Road 2", "street": "Abbey Road", "city": "London"}, "geometry": {"type": "Point", "coordinates": [0.0736776, 51.5374693]}}]}'


def test_form_submit_with_wrong_key_does_not_fill_result_table_and_logs_error(qgisapify: GeoapifyPlugin,
                                                                              qtbot):
    def checkErrorMessageSubmitted(msg, _tag, level):
        return level == Qgis.MessageLevel.Warning \
            and msg == "Search failed. Check that your API key is correct."

    qtbot.keyClicks(
        qgisapify.dockwidget.addressLineEdit,
        "Wimbledon, UK"
    )
    qtbot.keyClicks(
        qgisapify.dockwidget.apiKeyLineEdit,
        "WRONG_API_KEY"
    )

    message_recv_callback = QgsApplication.messageLog().messageReceived

    with qtbot.waitSignal(message_recv_callback, check_params_cb=checkErrorMessageSubmitted):
        qtbot.mouseClick(
            qgisapify.dockwidget.searchButton,
            Qt.MouseButton.LeftButton
        )
