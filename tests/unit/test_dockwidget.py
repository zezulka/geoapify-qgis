from unittest.mock import Mock

import pytest
from PyQt5.QtCore import QSettings
from qgis._core import QgsSettings
from qgis.gui import QgisInterface

from qgisapify.client.request import GeocodeSearchRequest
from qgisapify.constants import QGIS_PLUGIN_NAME
from qgisapify.geoapify_plugin_dockwidget import GeoapifyPluginDockWidget
from qgisapify.worker.search_worker import GeocodeSearchWorker
from tests.unit.fixtures import createGeoDataFrameFromExampleDict, abbey_road_raw_dict


@pytest.fixture
def qgs_settings() -> QgsSettings:
    s = QgsSettings(QSettings.UserScope, organization=QGIS_PLUGIN_NAME, application=QGIS_PLUGIN_NAME)
    s.setValue(f"/{QGIS_PLUGIN_NAME}/geoapify/api/urlprefix", "http://")
    return s


@pytest.fixture
def dockwidget(qgis_iface: QgisInterface,
               qgs_settings: QgsSettings,
               worker) -> GeoapifyPluginDockWidget:
    wid = GeoapifyPluginDockWidget(iface=qgis_iface, settings=qgs_settings)
    wid._worker = worker
    yield wid


@pytest.fixture
def dockwidget_with_filled_in_form(dockwidget: GeoapifyPluginDockWidget) -> GeoapifyPluginDockWidget:
    dockwidget.addressLineEdit.setText("Wimbledon, UK")
    dockwidget.apiKeyLineEdit.setText("615fefff9d32d38057bf33d8a94eecff")
    return dockwidget


@pytest.fixture
def worker():
    worker = Mock(GeocodeSearchWorker)
    worker.start.return_value = createGeoDataFrameFromExampleDict(abbey_road_raw_dict)
    return worker


@pytest.fixture
def fully_setup_dockwidget(dockwidget_with_filled_in_form: GeoapifyPluginDockWidget) -> GeoapifyPluginDockWidget:
    return dockwidget_with_filled_in_form


class TestFormValidation:
    def test_widget_should_have_search_button_turned_off_by_default(self, dockwidget: GeoapifyPluginDockWidget):
        assert not dockwidget.searchButton.isEnabled()

    def test_search_button_enabled_when_address_and_api_key_nonempty(self, dockwidget: GeoapifyPluginDockWidget):
        dockwidget.addressLineEdit.setText("Wimbledon, UK")
        dockwidget.apiKeyLineEdit.setText("615fefff9d32d38057bf33d8a94eecff")

        assert dockwidget.searchButton.isEnabled()

    def test_search_button_disabled_even_if_it_was_enabled_before(self, dockwidget: GeoapifyPluginDockWidget):
        dockwidget.addressLineEdit.setText("Wimbledon, UK")
        dockwidget.apiKeyLineEdit.setText("615fefff9d32d38057bf33d8a94eecff")

        assert dockwidget.searchButton.isEnabled()

        dockwidget.addressLineEdit.clear()

        assert not dockwidget.searchButton.isEnabled()

    def test_search_button_disabled_when_address_empty(self, dockwidget: GeoapifyPluginDockWidget):
        dockwidget.apiKeyLineEdit.setText("615fefff9d32d38057bf33d8a94eecff")

        assert not dockwidget.searchButton.isEnabled()

    def test_search_button_disabled_when_api_key_empty(self, dockwidget: GeoapifyPluginDockWidget):
        dockwidget.addressLineEdit.setText("Wimbledon, UK")

        assert not dockwidget.searchButton.isEnabled()

    def test_search_button_disabled_when_api_key_blank(self, dockwidget: GeoapifyPluginDockWidget):
        dockwidget.addressLineEdit.setText("Wimbledon, UK")
        dockwidget.apiKeyLineEdit.setText("            ")

        assert not dockwidget.searchButton.isEnabled()

    def test_search_button_disabled_when_address_blank(self, dockwidget: GeoapifyPluginDockWidget):
        dockwidget.addressLineEdit.setText("            ")
        dockwidget.apiKeyLineEdit.setText("615fefff9d32d38057bf33d8a94eecff")

        assert not dockwidget.searchButton.isEnabled()


class TestSearch:
    def test_search_calls_worker(
            self,
            fully_setup_dockwidget: GeoapifyPluginDockWidget
    ):
        # emit the click signal to the search button
        fully_setup_dockwidget.searchButton.click()

        self._assert_worker_called_with_expected_request(fully_setup_dockwidget)

    def _assert_worker_called_with_expected_request(self, fully_setup_dockwidget: GeoapifyPluginDockWidget):
        """
        We can't assert that data were actually populated as this requires further cooperation with
        the worker -> we need to inspect this cross-component behaviour in an integration test.
        """
        fully_setup_dockwidget._worker.start.assert_called_with(
            GeocodeSearchRequest(api_key='615fefff9d32d38057bf33d8a94eecff',
                                 address='Wimbledon, UK',
                                 api_prefix_url='http://'))
