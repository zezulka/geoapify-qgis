import geopandas as gpd

from qgisapify.constants import GEOAPIFY_CRS

abbey_road_raw_dict = {
    "type": "FeatureCollection",
    "features": [
        {
            "id": "0",
            "type": "Feature",
            "properties": {
                "datasource": {
                    "sourcename": "openstreetmap",
                    "attribution": "© OpenStreetMap contributors",
                    "license": "Open Database License",
                    "url": "https://www.openstreetmap.org/copyright"
                },
                "name": "Abbey Road",
                "ref": "A123",
                "country": "United Kingdom",
                "country_code": "gb",
                "state": "England",
                "county": "Greater London",
                "city": "London",
                "postcode": "IG11 7EP",
                "district": "London Borough of Barking and Dagenham",
                "neighbourhood": "Roding Riverside",
                "street": "Abbey Road",
                "lon": 0.0736776,
                "lat": 51.5374693,
                "state_code": "ENG",
                "result_type": "street",
                "formatted": "Abbey Road, London, IG11 7EP, United Kingdom",
                "address_line1": "Abbey Road",
                "address_line2": "London, IG11 7EP, United Kingdom",
                "timezone": {
                    "name": "Europe/London",
                    "offset_STD": "+00:00",
                    "offset_STD_seconds": 0,
                    "offset_DST": "+01:00",
                    "offset_DST_seconds": 3600,
                    "abbreviation_STD": "GMT",
                    "abbreviation_DST": "BST"
                },
                "plus_code": "9F32G3PF+XF",
                "plus_code_short": "G3PF+XF London, Greater London, United Kingdom",
                "rank": {
                    "importance": 0.41001,
                    "popularity": 7.838684191955424,
                    "confidence": 1,
                    "confidence_city_level": 1,
                    "confidence_street_level": 1,
                    "match_type": "full_match"
                },
                "place_id": "5107a1720289dcb23f59500d45cbcbc44940f00102f901e00ee50200000000c0020492030a416262657920526f6164",
                "suburb": None
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    0.0736776,
                    51.5374693
                ]
            }
        },
        {
            "id": "1",
            "type": "Feature",
            "properties": {
                "datasource": {
                    "sourcename": "openstreetmap",
                    "attribution": "© OpenStreetMap contributors",
                    "license": "Open Database License",
                    "url": "https://www.openstreetmap.org/copyright"
                },
                "name": "Abbey Road 2",
                "ref": "A123",
                "country": "United Kingdom",
                "country_code": "gb",
                "state": "England",
                "county": "Greater London",
                "city": "London",
                "postcode": "IG11 7EP",
                "district": "London Borough of Barking and Dagenham",
                "neighbourhood": "Roding Riverside",
                "street": "Abbey Road",
                "lon": 0.0736776,
                "lat": 51.5374693,
                "state_code": "ENG",
                "result_type": "street",
                "formatted": "Abbey Road, London, IG11 7EP, United Kingdom",
                "address_line1": "Abbey Road",
                "address_line2": "London, IG11 7EP, United Kingdom",
                "timezone": {
                    "name": "Europe/London",
                    "offset_STD": "+00:00",
                    "offset_STD_seconds": 0,
                    "offset_DST": "+01:00",
                    "offset_DST_seconds": 3600,
                    "abbreviation_STD": "GMT",
                    "abbreviation_DST": "BST"
                },
                "plus_code": "9F32G3PF+XF",
                "plus_code_short": "G3PF+XF London, Greater London, United Kingdom",
                "rank": {
                    "importance": 0.41001,
                    "popularity": 7.838684191955424,
                    "confidence": 1,
                    "confidence_city_level": 1,
                    "confidence_street_level": 1,
                    "match_type": "full_match"
                },
                "place_id": "5107a1720289dcb23f59500d45cbcbc44940f00102f901e00ee50200000000c0020492030a416262657920526f6164",
                "suburb": None
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    0.0736776,
                    51.5374693
                ]
            }
        }
    ]
}


def createGeoDataFrameFromExampleDict(geoapify_dict: dict) -> gpd.GeoDataFrame:
    return gpd.GeoDataFrame.from_features(geoapify_dict, crs=GEOAPIFY_CRS)
