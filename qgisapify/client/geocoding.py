from typing import Dict, Union

import requests
from geobatchpy.utils import API_GEOCODE
from geopandas import GeoDataFrame
from qgis.core import QgsMessageLog, Qgis

from qgisapify.constants import GEOAPIFY_CRS


def search(query: str,
           api_key: str,
           api_prefix_url: str,
           lang: str = "en") -> Union[GeoDataFrame | None]:
    """
    Geocode an address using the Geoapify API.

    :param str query: The text-based address query. No address format is assumed.
    :param str api_key: Your hexadecimal Geoapify API key with Geocoding API functionality enabled.
    :param str api_prefix_url: API prefix URL; useful for integration tests
    :param str lang: The language of the geocoding results. Defaults to "en" (English).
    """
    r = geocode(
        text=query,
        parameters={'lang': lang},
        api_key=api_key,
        api_prefix_url=api_prefix_url
    )
    if 'error' in r:
        return QgsMessageLog.logMessage(
            message=str(r),
            level=Qgis.Warning
        )
    else:
        return GeoDataFrame.from_features(r, crs=GEOAPIFY_CRS)


def get_api_url(api: str,
                api_key: str,
                api_prefix_url: str,
                version: int = None) -> str:
    """
    Geoapify client hardcodes prefix URL :'((
    The code below is just a copypaste with variable prefix URL
    """
    if version is None:
        # Use version as defined above
        return f'{api_prefix_url}{api}?apiKey={api_key}'
    else:
        api = f'/v{version}/' + '/'.join(api.split('/')[2:])
        return f'{api_prefix_url}{api}?apiKey={api_key}'


def geocode(api_key: str,
            api_prefix_url: str,
            headers: Dict[str, str] = None,
            text: str = None,
            parameters: Dict[str, str] = None) -> dict:
    if headers is None:
        headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}
    request_url = get_api_url(api=API_GEOCODE, api_key=api_key, api_prefix_url=api_prefix_url)

    params = {'text': text} if text is not None else dict()
    if parameters is not None:
        params = {**params, **parameters}

    return requests.get(url=request_url, params=params, headers=headers).json()
