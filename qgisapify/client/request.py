from dataclasses import dataclass


@dataclass
class GeocodeSearchRequest:
    api_key: str
    address: str
    api_prefix_url: str
