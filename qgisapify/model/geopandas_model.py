import numpy as np
from PyQt5.QtCore import Qt, QModelIndex
from geopandas import GeoDataFrame
from qgis.PyQt import QtCore


class GeopandasModel(QtCore.QAbstractTableModel):
    """
    Class to populate a table view with a pandas dataframe
    """
    def __init__(self, data: GeoDataFrame, parent=None):
        QtCore.QAbstractTableModel.__init__(self, parent)
        self._data = data

    def geometry(self, row: int):
        return self._data.iat[row, -1]

    def rowCount(self, parent=None, *args, **kwargs):
        return len(self._data.values)

    def columnCount(self, parent=None, *args, **kwargs):
        # take away the geometry column
        return self._data.columns.size - 1

    def data(self, index: QModelIndex, role=QtCore.Qt.DisplayRole):
        if index.isValid() and role == QtCore.Qt.DisplayRole:
            index_val = self._data.iat[index.row(), index.column()]
            return str(index_val) if index_val else None
        else:
            return None

    def headerData(self, col: int, orientation: Qt.Orientation, role=None):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            return self._data.columns[col]
        else:
            return None
