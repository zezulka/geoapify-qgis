from typing import List

from geopandas import GeoDataFrame
import numpy


def normalizeGeoDataFrame(original_df: GeoDataFrame, columns: List[str] = None) -> GeoDataFrame:
    if columns is None:
        columns = ['name', 'street', 'city']
    return GeoDataFrame(
        data=original_df,
        geometry=original_df.geometry,
        crs=original_df.crs,
        columns=columns
    ).replace((numpy.nan, ''), (None, None))
