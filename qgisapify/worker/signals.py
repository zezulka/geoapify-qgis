from traceback import format_exc
from typing import Callable

from PyQt5.QtCore import QRunnable, QObject, pyqtSignal


class QgisapifyQRunnableSignals(QObject):
    """
    Defines the signals available from a running worker thread.
    """
    finished = pyqtSignal()
    start = pyqtSignal()
    error = pyqtSignal(object)
    success = pyqtSignal(object)

