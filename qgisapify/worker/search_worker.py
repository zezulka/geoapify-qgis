from traceback import format_exc
from typing import Callable, Union

from PyQt5.QtCore import QThreadPool, qCritical
from geopandas import GeoDataFrame
from qgis.core import QgsMessageLog, Qgis
from qgis.gui import QgisInterface

from qgisapify.client.geocoding import search
from qgisapify.client.request import GeocodeSearchRequest
from qgisapify.utils.dataframe import normalizeGeoDataFrame
from qgisapify.worker.base import QgisapifyQRunnable
from qgisapify.worker.errors import SearchFailed
from qgisapify.worker.signals import QgisapifyQRunnableSignals


class GeocodeSearchWorker:
    def __init__(self,
                 iface: QgisInterface,
                 success_callback: Callable[[GeoDataFrame], None]):
        self._iface = iface
        self._threadpool = QThreadPool.globalInstance()
        self._layer_import_signals = QgisapifyQRunnableSignals()
        self._layer_import_signals.start.connect(self._on_start)
        self._layer_import_signals.success.connect(success_callback)
        self._layer_import_signals.error.connect(self._on_error)

    def start(self, search_request: GeocodeSearchRequest):
        worker = QgisapifyQRunnable(self._search, self._layer_import_signals, search_request)
        self._threadpool.start(worker)

    def _search(self, search_request: GeocodeSearchRequest) -> Union[None | GeoDataFrame]:
        """
        Logic to be executed inside the worker.
        """
        geoframe = search(api_key=search_request.api_key,
                          query=search_request.address,
                          api_prefix_url=search_request.api_prefix_url)
        if geoframe is not None:
            return normalizeGeoDataFrame(original_df=geoframe)
        else:
            QgsMessageLog.logMessage("Search failed. Check that your API key is correct.",
                                     level=Qgis.MessageLevel.Warning)
            raise SearchFailed(search_request)

    def _on_start(self):
        QgsMessageLog.logMessage("Searching for interesting places...", level=Qgis.MessageLevel.Info)

    def _on_error(self, exception):
        print(exception)
        print(format_exc())
        qCritical(str(exception))
        qCritical(format_exc())
        QgsMessageLog.logMessage(f"Unknown error: {format_exc()}", level=Qgis.MessageLevel.Critical)
