from traceback import format_exc
from typing import Callable

from PyQt5.QtCore import QRunnable

from qgisapify.worker.signals import QgisapifyQRunnableSignals


class QgisapifyQRunnable(QRunnable):
    """
    Worker thread
    Inherits from QRunnable to handler worker thread setup, signals and wrap-up.
    """

    def __init__(self, fn: Callable, signals: QgisapifyQRunnableSignals, *args):
        super(QgisapifyQRunnable, self).__init__()
        # Store constructor arguments (re-used for processing)
        self.fn = fn
        self.args = args
        self.signals = signals

    def run(self):
        try:
            self.signals.start.emit()
            r = self.fn(*self.args)
            self.signals.success.emit(r)
        except Exception as e:
            # This is a VERY naive implementation of exception handling
            # modify to your needs
            print(format_exc())
            self.signals.error.emit(e)
        finally:
            self.signals.finished.emit()